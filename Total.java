class Person{
    String name1 = "anyone";
    void displaymsg1(){
        System.out.println("Person is called ");
    }
   
}
class Faculty extends Person{
    String name2 = "sir";
    void displaymsg2(){
        System.out.println("Faculty called ");
    }
}
class People extends Person{
    String name3 = "student";
    void displaymsg3(){
        System.out.println("Student called ");
    }
   
}
class Total extends Faculty{
    String name4 = "total";
    void displaymsg4(){
        System.out.println("Total called ");
    }
    public static void main(String[] args){
        Total obj = new Total();
        obj.displaymsg1();
        obj.displaymsg2();
        obj.displaymsg4();
    }
}
