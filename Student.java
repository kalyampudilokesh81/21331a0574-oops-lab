class Student{
    String fname;
    double sempercent;
    String cname;
    int ccode;
    Student(){
        cname = "MVGR";
        ccode = 33;
        System.out.println("The College name and College code is : ");
        System.out.println(cname);
        System.out.println(ccode);
    }
    Student(String fullname,double sempercentage){
        fname = fullname;
        sempercent = sempercentage;
        System.out.println("NAME : " + fname + "  PERCENT : " + sempercent);
    }
    protected void finalize(){
        System.out.println("Dead");
    }
    public static void main(String[] args){
        Student obj1 = new Student();
        Student obj2 = new Student("Lokesh",80);
        obj1.finalize();
        obj2.finalize();
    }
}
