abstract class Class  
{  
    abstract void display();  
}  
public class MainClass extends Class
{    
    void display()  
    {  
        System.out.println("Abstract method called.");  
    }  
    public static void main(String[] args)  
    {    
        MainClass obj = new MainClass ();  
        obj.display();  
    }  
}    
