import java.text.*;
import java.util.Calendar;
 
public class ToPattern {
    public static void main(String[] args)
        throws InterruptedException
    {
        SimpleDateFormat SDformat
            = new SimpleDateFormat();
 
        // Initializing Calendar object
        Calendar cal = Calendar.getInstance();
 
        // Getting the Current Date
        String Todaysdate
            = SDformat.format(cal.getTime());
 
        // Displaying the date
        System.out.println("Current Date: "
                           + Todaysdate);
 
        // Using toPattern() method
        // to Print the Date Pattern
        System.out.println("The Date Pattern- "
                           + SDformat.toPattern());
    }
}