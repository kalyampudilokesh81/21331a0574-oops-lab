// Java program to demonstrate equals() method

import java.text.*;
import java.util.*;
import java.io.*;

public class EqualsMethod {
	public static void main(String[] argv)
	{
		// creating and initializing ChoiceFormat
		ChoiceFormat cf1
			= new ChoiceFormat(
				"1#sun| 2#mon | 3#tues | 4#wed");

		// creating and initializing ChoiceFormat
		ChoiceFormat cf2
			= new ChoiceFormat(
				"4#wed| 5#thu | 6#fri | 7#sat");

		// compare cf1 and cf2
		// using equals() method
		boolean status = cf1.equals(cf2);

		// display the result
		if (status)
			System.out.println("Both ChoiceFormat"
							+ " objects are equal");
		else
			System.out.println("Both ChoiceFormat "
							+ "objects are not equal");
	}
}
