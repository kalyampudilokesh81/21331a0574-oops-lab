// Java program to demonstrate applyPattern() method

import java.text.*;
import java.util.*;
import java.io.*;

public class GFG {
	public static void main(String[] argv)
	{
		// creating and initializing limit
		double[] limit = { 1, 2, 3 };

		// creating and initializing format
		String[] format = { "sun", "mon", "tue" };

		// creating and initializing ChoiceFormat
		ChoiceFormat cf
			= new ChoiceFormat(limit, format);

		// display the result
		System.out.println("current pattern : "
						+ cf.toPattern());

		// applying the new pattern
		// using applyPattern() method
		cf.applyPattern("4#wed| 5#thu | 6#fri | 7#sat");

		// display the result
		System.out.println("\nnew pattern : "
						+ cf.toPattern());
	}
}
