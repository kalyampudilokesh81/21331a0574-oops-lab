#include <iostream>
#include <iomanip>


using namespace std;


int main()
{
    int num1 = 10;
    double num2 = 3.14159;
    cout << "This is some text " << endl;
    cout << "nothing here" << flush;
    cout << setw(10) << setfill('*') << num1 << endl;
    cout << setw(10) << setfill('#') << num2 << endl;
    cout << setprecision(2) << num2 << endl;


    return 0;
}
