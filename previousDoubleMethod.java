

import java.text.*;
import java.util.*;
import java.io.*;

public class previousDoubleMethod {
	public static void main(String[] argv)
	{
		// getting double value just
		// lesser than the passed value
		// using previousDouble() method
		double value
			= ChoiceFormat.previousDouble(22);

		// display the result
		System.out.print("Lesser double value: "
						+ value);
	}
}
