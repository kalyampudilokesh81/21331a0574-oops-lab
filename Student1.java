class Student1{
    String fName;
    int rNum;
    double semPercent;
    String cName;
    int cCode;
    Student1(String fullName,int rollNum,double semPercentage,String collegeName,int collegeCode){
        fName = fullName;
        rNum = rollNum;
        semPercent = semPercentage;
        cName = collegeName;
        cCode = collegeCode;
    }
    void displayMsg(){
        System.out.println("Full name : " +fName);
        System.out.println("Roll number : " +rNum);
        System.out.println("Sem percentage : " +semPercent);
        System.out.println("College name : " +cName);
        System.out.println("College code : " +cCode);
    }
    protected void finalize(){
        System.out.println("Dead");
    }
    public static void main(String[] args){
        Student1 obj1 = new Student1("Lokesh",74,80,"MVGR",33);
        Student1 obj2 = new Student1("Kanth",73,80,"MVGR",33);
        obj1.displayMsg();
        obj2.displayMsg();
        obj1.finalize();
        obj2.finalize();
    }
}
