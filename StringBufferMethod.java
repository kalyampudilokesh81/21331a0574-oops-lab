import java.text.ChoiceFormat;
import java.text.FieldPosition;

public class StringBufferMethod {
    public static void main(String[] args) {
        double[] limits = {1, 2, 3};
        String[] grades = {"A", "B", "C"};

        ChoiceFormat choiceFormat = new ChoiceFormat(limits, grades);

        double number = 2.5;
        StringBuffer stringBuffer = new StringBuffer();
        FieldPosition fieldPosition = new FieldPosition(0);

        choiceFormat.format(number, stringBuffer, fieldPosition);

        System.out.println("Formatted grade: " + stringBuffer.toString());
    }
}
