public class Boxing {
    public static void main(String[] args) {
        //boxing
       Integer num1 = new Integer(9);
        System.out.println(num1);
        //unboxing
        int num2 = num1;
        System.out.println(num2);
    }
